package ListaLigadaDEC;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Giliardi Schmidt
 */
public class ListaTest {

    private Lista<Integer> l = new Lista();

    @Before
    public void before() {
        for (int i = 0; i < 10; i++) {
            l.add(i);
        }
    }

    @After
    public void tearDown() {
        l.clear();
    }

    /**
     * Test of size method, of class Lista.
     */
    @Test
    public void testSize() {
        assertEquals(l.size(), 10);

        l.clear();
        assertEquals(l.size(), 0);
    }

    /**
     * Test of isEmpty method, of class Lista.
     */
    @Test
    public void testIsEmpty() {
        assertFalse(l.isEmpty());

        l.clear();
        assertTrue(l.isEmpty());
    }

    /**
     * Test of contains method, of class Lista.
     */
    @Test
    public void testContains() {
        assertFalse(l.contains(11));
        assertTrue(l.contains(0));
        assertTrue(l.contains(9));
    }

    /**
     * Test of iterator method, of class Lista.
     */
    @Test
    public void testIterator() {
        for (int i = 0; i < 10; i++) {
            Integer a = i;
            assertEquals(l.get(i), a);
        }
    }

    /**
     * Test of add method, of class Lista.
     */
    @Test
    public void testAdd_Object() {
        Integer exp = 1;
        assertEquals(l.get(1), exp);
    }

    /**
     * Test of remove method, of class Lista.
     */
    @Test
    public void testRemove_Object() {
        assertEquals(l.size(), 10);
        Integer a = 4;
        l.remove(a);
        assertEquals(l.size(), 9);
    }

    /**
     * Test of getLast method, of class Lista.
     */
    @Test
    public void testGetLast() {
        assertEquals(l.getLast(), 9);
    }

    /**
     * Test of addAll method, of class Lista.
     */
    @Test
    public void testAddAll_Collection() {
        Lista<Integer> k = new Lista();
        l.clear();

        for (int i = 0; i < 10; i++) {
            k.add(i);
        }
        l.addAll(k);

        for (int i = 0; i < 10; i++) {
            Integer a = i;
            assertEquals(l.get(i), a);
        }

    }

    /**
     * Test of clear method, of class Lista.
     */
    @Test
    public void testClear() {
        assertFalse(l.isEmpty());
        l.clear();
        assertTrue(l.isEmpty());
    }

    /**
     * Test of get method, of class Lista.
     */
    @Test
    public void testGet() {
        assertEquals(l.get(0), 0);
        assertEquals(l.get(9), 9);
    }

    /**
     * Test of set method, of class Lista.
     */
    @Test
    public void testSet() {
        l.set(5, 100);
        assertEquals(l.get(5), 100);
    }

    /**
     * Test of add method, of class Lista.
     */
    @Test
    public void testAdd_int_Object() {
        assertEquals(l.get(5), 5);
        l.add(5, 100);
        assertEquals(l.get(5), 100);
        assertEquals(l.get(6), 5);
        assertEquals(l.size(), 11);
    }

    /**
     * Test of remove method, of class Lista.
     */
    @Test
    public void testRemove_int() {
        assertEquals(l.remove(5), 5);
        assertEquals(l.get(5), 6);
        assertEquals(l.size(), 9);
    }

    /**
     * Test of indexOf method, of class Lista.
     */
    @Test
    public void testIndexOf() {
        assertEquals(l.indexOf(7), 7);
    }

    /**
     * Test of getFirst method, of class Lista.
     */
    @Test
    public void testGetFirst() {
        assertEquals(l.getFirst(), 0);
    }

    /**
     * Test of addFirst method, of class Lista.
     */
    @Test
    public void testAddFirst() {
        l.addFirst(100);
        assertEquals(l.get(0), 100);
        assertEquals(l.get(1), 0);
        assertEquals(l.size(), 11);

    }

    /**
     * Test of removeFirst method, of class Lista.
     */
    @Test
    public void testRemoveFirst() {
        assertEquals(l.removeFirst(), 0);
        assertEquals(l.get(0), 1);
        assertEquals(l.size(), 9);
    }

}
