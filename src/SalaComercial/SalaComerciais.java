package SalaComercial;


import static Auxiliares.EntradasTeclado.*;
import static Auxiliares.PreencheCampos.*;
import Default.ListaImovel;
import Default.TipoImovel;
import java.util.List;

/**
 *
 * @author Leônidas
 */
public class SalaComerciais {

    private TipoImovel tipo = TipoImovel.SALASCOMERCIAIS;
    private ListaImovel lista = new ListaImovel(tipo);

    /**
     * Metodo do menu principal.
     *
     * Deve ser chamado para que alterações possam ser feitas nas Salas
     * Comerciais.
     */
    public void principal() {
        int opcao;
        do {
            opcao = inInt("Digite:"
                    + "\n 1 - Para incluir uma Sala Comercial"
                    + "\n 2 - Para listar as Salas Comerciais cadastrados"
                    + "\n 3 - Para editar uma Sala Comercial"
                    + "\n 4 - Para excluir uma Sala Comercial"
                    + "\n 5 - Para pesquisar"
                    + "\n 0 - Para sair");

            switch (opcao) {
                case 0:
                    lista.escreverArquivo();
                    break;
                case 1:
                    incluir();
                    break;
                case 2:
                    listar();
                    break;
                case 3:
                    editar();
                    break;
                case 4:
                    excluir();
                    break;
                case 5:
                    pesquisar();
                    break;
               
                default:
                    msg("Opção incorreta!");
            }

        } while (opcao != 0);

    }

    /**
     * Metodo responsavel por solicitar os dados, criar e adicionar na lista uma
     * sala comercial
     *
     */
    private void incluir() {
        String logradouro, bairro, cidade, descricao, nomeEdificio;
        int numero, nroSala, andar,nroBanheiro;
        double areaTotal, valor,valorCondominio;

        logradouro = preencheLogradouro();
        bairro = preencheBairro();
        cidade = preencheCidade();
        descricao = preencheDescricao();
        nomeEdificio = preencheNomeEdificio();
        numero = preencheNumero();
        nroSala = preencheNroSala();
        andar = preencheAndar();
        valorCondominio = preencheValorCondominio();
        areaTotal = preencheAreaTotal();
        valor = preencheValor();
        nroBanheiro = preencheNroBanheiro();

        SalaComercial salacomercial = new SalaComercial (logradouro,numero,bairro,
                cidade,descricao,nroBanheiro,areaTotal,valor,
                nroSala , valorCondominio,nomeEdificio, andar);

        if (lista.incluir(salacomercial)) {
            msg("Sala comercial adicionado com sucesso!");
        } else {
            msg("Ocorreu algum problema, tente novamente!");
        }
    }

    /**
     * Metodo responsavel pela consulta de uma sala comercial, pelo seu codigo.
     *
     * Pede ao usuario qual codigo deseja buscar e exibe os dados, caso seja
     * encontrado
     */
    private void pesquisarCodigo() {
        int codigo;

        codigo = preencheCodigo();
        if (codigo < 0) {
            msg("Codigo incorreto!");
        }
        SalaComercial salaComercial = (SalaComercial) lista.consultar(codigo);

        if (salaComercial == null) {
            msg("Nenhuma sala comercial encontrada!");
        } else {
            msg(salaComercial.toString());
        }
    }
    /**
     * Metodo responsavel pelo menu principal de pesquisa. Redireciona para o
     * modo de pesquisa que o usuario escolher
     */
    private void pesquisar() {
        int opcao;;

        opcao = inInt("Digite:"
                + "\n 1 - Para pesquisar por codigo"
                + "\n 2 - Para pesquisar por valor"
                + "\n 3 - Para pesquisar por bairro"
                + "\n 0 - Para cancelar");

        switch (opcao) {
            case 0:
                break;
            case 1:
                pesquisarCodigo();
                break;
            case 2:
                pesquisarValor();
                break;
            case 3:
                pesquisarBairro();
                break;
            default:
                msg("Opção incorreta!");
                break;
        }
    }

    
    /**
     * Metodo responsavel pela pesquisa de um sala comercial, pelo valor.
     *
     * Pede ao usuario qual valor maximo que deseja buscar e exibe a lista de
     * imoveis encontrados.
     */
    private void pesquisarValor() {
        List salaComerciais;
        double valor;

        valor = inDouble("Digite o valor máximo que deseja pesquisar:");
        salaComerciais = lista.pesquisaValor(valor);
        exibirSalaComercial(salaComerciais);
    }
    
    /**
     * Metodo responsavel pela pesquisa de uma sala comercial, pelo bairro.
     *
     * Pede ao usuario qual bairro deseja buscar e exibe a lista de imoveis
     * encontrados.
     */
    private void pesquisarBairro() {
        String bairro;
        List salaComerciais;

        bairro = preencheBairro();
        salaComerciais = lista.pesquisaBairro(bairro);
        exibirSalaComercial(salaComerciais);
    }
    
    /**
     * Metodo responsavel pelo menu apos pesquisar e listar.
     */
    private void menuLista() {
        int codigo;

        codigo = inInt("Para exibir mais informações, digite o codigo correspondente a sala comercial."
                + "\n Digite -1 para sair");
        if (codigo < 0) {
            return;
        }
        SalaComercial salaComercial = (SalaComercial) lista.consultar(codigo);

        if (salaComercial == null) {
            msg("Nenhuma sala comercial encontrada!");
        } else {
            msg(salaComercial.toString());
        }

    }
    /**
     * Metodo responsavel pela exclusao, pelo seu codigo.
     *
     * Pede ao usuario para digitar o codigo da sala que deseja excluir.
     * 
     */
    private void excluir() {
        int codigo;

        codigo = preencheCodigo();
        if (lista.excluir(codigo)) {
            msg("Sala Comercial excluida com sucesso!");
        } else {
            msg("Sala Comercial não encontrada!");
        }
    }
       /**
     * Metodo responsavel pela edição de um salacomercial.
     *
     * Pede ao usuario qual o codigo do salacomercial que deseja editar. Caso seja
        encontrado, pede qual atributo deseja editar e qual o novo valor do
         atributo.
     Salva as alterações após o usuário escolher sair da edição.
     */
    private void editar() {
        SalaComercial salacomercial;
        int codigo, opcao;

        codigo = inInt("Digite o codigo do Sala Comercial que deseja editar: ");

        salacomercial = (SalaComercial) lista.consultar(codigo);
        if (salacomercial == null) {
            msg("Sala Comercial não encontrado!");
        } else {
            do {
                System.out.println(salacomercial.toString());
                opcao = inInt("Digite o numero correspondende ao item que você deseja editar: "
                        + "\n Digite 0 para sair ");

                switch (opcao) {
                    case 0:
                        break;
                    case 1:
                        salacomercial.setLogradouro(preencheLogradouro());
                        break;
                    case 2:
                        salacomercial.setNumero(preencheNumero());
                        break;
                    case 3:
                        salacomercial.setBairro(preencheBairro());
                        break;
                    case 4:
                        salacomercial.setCidade(preencheCidade());
                        break;
                    case 5:
                        salacomercial.setDescricao(preencheDescricao());
                        break;
                    case 6:
                        salacomercial.setAreaTotal(preencheAreaTotal());
                        break;
                    case 7:
                        salacomercial.setValor(preencheValor());
                        break;
                    case 8:
                        salacomercial.setNroBanheiro(preencheNroBanheiro());
                        break;
                    case 9:
                        salacomercial.setNomeEdificio(preencheNomeEdificio());
                        break;
                    case 10:
                        salacomercial.setAndar(preencheAndar());
                        break;
                    case 11:
                         salacomercial.setNroSala(preencheNroSala());
                        break;
                    case 12:
                        salacomercial.setValorCondominio((float) preencheValorCondominio());
                        break;
                    
                    default:
                        msg("Opcão incorreta!");
                        break;
                }
            } while (opcao != 0);
            if (lista.editar(codigo, salacomercial)) {
                msg("Sala Comercial editado com sucesso!");
            } else {
                msg("Ocorreu algum erro, tente novamente!");
            }
        }
    }

   
    /**
     * Metodo responsável por exibir todos os apartametos cadastrados, conforme
     * o usuario desejar.
     */
    private void listar() {
        int opcao;

        opcao = inInt("Digite:"
                + "\n 1 - Para listar por codigo"
                + "\n 2 - Para listar por valor"
                + "\n 3 - Para listar por area");

        switch (opcao) {
            case 0:
                break;
            case 1:
                listarCodigo();
                break;
            case 2:
                listarValor();
                break;
            case 3:
                listarArea();
                break;
            default:
                msg("Opcao incorreta!");
                break;
        }
    }
     /**
     * Metodo responsavel por listar por codigo.
     */
    private void listarCodigo() {
        List salaComerciais = lista.ordenarCodigo();
        exibirSalaComercial(salaComerciais);

    }

    /**
     * Metodo responsavel por ordenar por valor.
     */
    private void listarValor() {
        List salaComerciais = lista.ordenarValor();
        exibirSalaComercial(salaComerciais);
    }

    /**
     * Metodo responsavel por listar por area.
     */
    private void listarArea() {
        List salaComerciais = lista.ordenarArea();
        exibirSalaComercial(salaComerciais);
    }
   /**
     * Metodo responsavel por exibir uma descricao resumida das salas comerciais
     * contidos na List recebida.
     *
     * @param salaComercial List de apartamentos para exibir
     */
    private void exibirSalaComercial(List salaComerciais) {
        String dados = "";
        SalaComercial salaComercial;
        if (salaComerciais.get(0) == null) {
            msg("Nenhuma sala comercial encontrada!");
        } else {
            for (Object o : salaComerciais) {
                salaComercial = (SalaComercial) o;
                dados += "Codigo: " + salaComercial.getCodigo() + " \tCidade:" + salaComercial.getCidade()
                        + " \tBairro: " + salaComercial.getBairro() + " \tValor" + salaComercial.getValor()
                        + " \tArea Total: " + salaComercial.getAreaTotal() + "m" +"\n";
            }
            msg(dados);
            menuLista();
        }
    }
    
    public void ordenarArea(){
        for(int i= 0; i<lista.ordenarArea().size();i++){
            System.out.println("Area "+lista.ordenarArea().get(i).getAreaTotal());
        }
    }
    public void lerDatabase() {
        if (!lista.lerArquivo()) {
            msg("Ocorreu algum erro ao ler a base de arquivos. O arquivo nao existe ou esta corrompido!");
        }
    }

}
