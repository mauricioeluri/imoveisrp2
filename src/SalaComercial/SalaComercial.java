package SalaComercial;

import Default.Imovel;

/**
 *
 * @author Leônidas
 */
public class SalaComercial extends Imovel {

    private int nroBanheiro;
    private String nomeEdificio;
    private int andar;
    private int nroSala;
    private double valorCondominio;

    public SalaComercial(String logradouro, int numero, String bairro,
            String cidade, String descricao, int nroBanheiro, double areaTotal, double valor,
            int nroSala, double valorCondominio, String nomeEdificio,
            int andar) {
        super(logradouro, numero, bairro, cidade, descricao, areaTotal, valor);
        this.nroBanheiro = nroBanheiro;
        this.nomeEdificio = nomeEdificio;
        this.andar = andar;
        this.nroSala = nroSala;
        this.valorCondominio = valorCondominio;

    }

    public SalaComercial(String logradouro, int numero, String bairro,
            String cidade, String descricao, int nroBanheiro, double areaTotal, double valor,
            int nroSala, double valorCondominio, String nomeEdificio,
            int andar, int codigo) {
        super(logradouro, numero, bairro, cidade, descricao, areaTotal, valor, codigo);
        this.nroBanheiro = nroBanheiro;
        this.nomeEdificio = nomeEdificio;
        this.andar = andar;
        this.nroSala = nroSala;
        this.valorCondominio = valorCondominio;

    }

    /**
     * @return the nroBanheiro
     */
    public int getNroBanheiro() {
        return nroBanheiro;
    }

    /**
     * @param nroBanheiro the nroBanheiro to set
     */
    public void setNroBanheiro(int nroBanheiro) {
        this.nroBanheiro = nroBanheiro;
    }

    /**
     * @return the nomeEdificio
     */
    public String getNomeEdificio() {
        return nomeEdificio;
    }

    /**
     * @param nomeEdificio the nomeEdificio to set
     */
    public void setNomeEdificio(String nomeEdificio) {
        this.nomeEdificio = nomeEdificio;
    }

    /**
     * @return the andar
     */
    public int getAndar() {
        return andar;
    }

    /**
     * @param andar the andar to set
     */
    public void setAndar(int andar) {
        this.andar = andar;
    }

    /**
     * @return the nroSala
     */
    public int getNumeroSala() {
        return getNroSala();
    }

    /**
     * @return the valorCondominio
     */
    public double getValorCondominio() {
        return valorCondominio;
    }

    public void setValorCondominio(double valorCondominio) {
        this.valorCondominio = valorCondominio;
    }

    /**
     * @return the nroSala
     */
    public int getNroSala() {
        return nroSala;
    }

    /**
     * @param nroSala the nroSala to set
     */
    public void setNroSala(int nroSala) {
        this.nroSala = nroSala;
    }

    @Override
    public String toString() {
        String dados = "";
        dados += super.toString()
                + "\n8 - Numero de Banheiros: " + this.nroBanheiro
                + "\n9 - Nome do Edifício: " + this.nomeEdificio
                + "\n10 - Andar: " + this.andar
                + "\n11 - Numero da Sala: " + this.nroSala
                + "\n12 - Valor do Condominio: " + this.valorCondominio;

        return dados;
    }

    @Override
    public String toFile() {
        String dados = "";
        dados += super.toFile() + this.nroBanheiro + ";" + this.nomeEdificio + ";"
                + this.andar + ";" + this.nroSala + ";" + this.valorCondominio + ";";

        return dados;
    }

}
