package Chacara;

import Default.Imovel;

public class Chacara extends Imovel{

    private double areaConstruida;
    private int nroQuartos;
    private int anoConstrucao;
    private double distCidade;

    /**
     * Construtor de uma Chácara
     *
     * @param logradouro
     * @param numero
     * @param bairro
     * @param cidade
     * @param descricao
     * @param areaTotal
     * @param valor
     * @param areaConstruida
     * @param nroQuartos
     * @param anoConstrucao
     * @param distCidade
     */
    public Chacara(String logradouro, int numero, String bairro, String cidade, String descricao,
            double areaTotal,
            double valor, double areaConstruida, int nroQuartos,
            int anoConstrucao, double distCidade) {

        super(logradouro, numero, bairro, cidade, descricao, areaTotal, valor);
        this.areaConstruida = areaConstruida;
        this.nroQuartos = nroQuartos;
        this.anoConstrucao = anoConstrucao;
        this.distCidade = distCidade;
    }

    public Chacara(String logradouro, int numero, String bairro, String cidade, String descricao,
            double areaTotal,
            double valor, double areaConstruida, int nroQuartos,
            int anoConstrucao, double distCidade, int codigo) {

        super(logradouro, numero, bairro, cidade, descricao, areaTotal, valor, codigo);
        this.areaConstruida = areaConstruida;
        this.nroQuartos = nroQuartos;
        this.anoConstrucao = anoConstrucao;
        this.distCidade = distCidade;
    }

    /**
     * @return the areaConstruida
     */
    public double getAreaConstruida() {
        return areaConstruida;
    }

    /**
     * @param areaConstruida the areaConstruida to set
     */
    public void setAreaConstruida(double areaConstruida) {
        this.areaConstruida = areaConstruida;
    }

    /**
     * @return the nroQuartos
     */
    public int getNroQuartos() {
        return nroQuartos;
    }

    /**
     * @param nroQuartos the nroQuartos to set
     */
    public void setNroQuartos(int nroQuartos) {
        this.nroQuartos = nroQuartos;
    }

    /**
     * @return the anoConstrucao
     */
    public int getAnoConstrucao() {
        return anoConstrucao;
    }

    /**
     * @param anoConstrucao the anoConstrucao to set
     */
    public void setAnoConstrucao(int anoConstrucao) {
        this.anoConstrucao = anoConstrucao;
    }

    /**
     * @return the distCidade
     */
    public double getDistCidade() {
        return distCidade;
    }

    /**
     * @param distCidade the distCidade to set
     */
    public void setDistCidade(double distCidade) {
        this.distCidade = distCidade;
    }

    @Override
    public String toString() {
        String dados = "";
        dados += super.toString()
                + "\n8 - Área Construida: " + this.areaConstruida
                + "\n9 - Numero de Quartos: " + this.nroQuartos
                + "\n10 - Ano de Construção: " + this.anoConstrucao
                + "\n11 - Distância da Cidade: " + this.distCidade;

        return dados;
    }

    @Override
    public String toFile() {
        String dados = "";
        dados += super.toFile() + this.areaConstruida + ";" + this.nroQuartos + ";" + this.anoConstrucao + ";" + this.distCidade + ";";
        return dados;
    }
}
