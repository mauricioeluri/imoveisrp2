package Chacara;

import Default.ListaImovel;
import static Auxiliares.EntradasTeclado.*;
import static Auxiliares.PreencheCampos.*;
import Default.TipoImovel;
import java.util.List;

/**
 *
 * @author Filipe
 *
 * Classe responsável pelas modificações em Imoveis do tipo Chácara.
 */
public class Chacaras {

    private TipoImovel tipo = TipoImovel.CHACARAS;
    private ListaImovel lista = new ListaImovel(tipo);

    /**
     * Metodo do menu.
     *
     * Deve ser chamado para que alterações possam ser feitas nas Chácaras.
     */
    public void menu() {
        int opcao;
        do {
            opcao = inInt("Digite:"
                    + "\n 1 - Incluir Chácara:"
                    + "\n 2 - Listar Chácaras cadastradas:"
                    + "\n 3 - Editar uma Chácara:"
                    + "\n 4 - Excluir uma Chácara:"
                    + "\n 5 - Para pesquisar"
                    + "\n 0 - Sair");

            switch (opcao) {
                case 0:
                    lista.escreverArquivo();
                    break;
                case 1:
                    incluir();
                    break;
                case 2:
                    listar();
                    break;
                case 3:
                    editar();
                    break;
                case 4:
                    excluir();
                    break;
                case 5:
                    pesquisar();
                    break;
                default:
                    msg("Opção incorreta!");
            }

        } while (opcao != 0);

    }

    /**
     * Metodo responsavel por solicitar os dados, criar e adicionar na lista uma
     * chácara.
     */
    public void incluir() {
        String logradouro, bairro, cidade, descricao;
        int nroQuartos, anoConstrucao, numero;
        double distCidade, areaConstruida, areaTotal, valor;

        logradouro = preencheLogradouro();
        bairro = preencheBairro();
        cidade = preencheCidade();
        descricao = preencheDescricao();
        numero = preencheNumero();
        areaTotal = preencheAreaTotal();
        valor = preencheValor();
        nroQuartos = preencheNroQuartos();
        anoConstrucao = preencheAnoConstrucao();
        distCidade = preencheDistCidade();
        areaConstruida = preencheAreaConstruida();

        Chacara chacara = new Chacara(logradouro, numero, bairro, cidade,
                descricao, areaTotal, valor, areaConstruida, nroQuartos, anoConstrucao,
                distCidade);

        if (lista.incluir(chacara)) {
            msg("Chácara adicionada com sucesso!");
            System.out.println(chacara.toString());
        } else {
            msg("Ocorreu algum problema, tente novamente!");
        }

    }

    /**
     * Metodo responsavel pela consulta de uma Chácara.
     *
     * Pede ao usuario qual codigo deseja buscar e exibe os dados, caso seja
     * encontrado, da chácara.
     */
    private void consultarCodigo() {
        int codigo;

        codigo = preencheCodigo();
        Chacara chacara = (Chacara) lista.consultar(codigo);

        if (chacara == null) {
            msg("Nenhuma chácara encontrada!");
        } else {
            msg(chacara.toString());
        }
    }

    /**
     * Metodo responsavel pela exclusao de uma chácara.
     *
     * Pede ao usuario para digitar o codigo da chácara que deseja excluir.
     */
    private void excluir() {
        int codigo;

        codigo = inInt("Digite o codigo da Chácara que voce deseja Excluir:");
        if (lista.excluir(codigo)) {
            msg("Chácara excluida com sucesso!");
        } else {
            msg("Chácara não encontrada!");
        }
    }

    /**
     * Metodo responsavel pela edição de uma chácara.
     *
     * Pede ao usuario qual o codigo da chácara que deseja editar. Caso seja
     * encontrado, pede qual atributo deseja editar e qual o novo valor do
     * atributo.
     *
     * Salva as alterações após o usuário escolher sair da edição.
     */
    private void editar() {
        Chacara chacara;
        int codigo, opcao;

        codigo = inInt("Digite o codigo da chácara que deseja editar:");

        chacara = (Chacara) lista.consultar(codigo);
        if (chacara == null) {
            msg("Chácara não encontrada!");
        } else {
            do {
                System.out.println(chacara.toString());
                opcao = inInt("Digite o número correspondende ao item que você deseja editar:"
                        + "\n Digite 0 para sair");

                switch (opcao) {
                    case 0:
                        break;
                    case 1:
                        chacara.setLogradouro(preencheLogradouro());
                        break;
                    case 2:
                        chacara.setNumero(preencheNumero());
                        break;
                    case 3:
                        chacara.setBairro(preencheBairro());
                        break;
                    case 4:
                        chacara.setCidade(preencheCidade());
                        break;
                    case 5:
                        chacara.setDescricao(preencheDescricao());
                        break;
                    case 6:
                        chacara.setAreaTotal(preencheAreaTotal());
                        break;
                    case 7:
                        chacara.setValor(preencheValor());
                        break;
                    case 8:
                        chacara.setAreaConstruida(preencheAreaConstruida());
                        break;
                    case 9:
                        chacara.setNroQuartos(preencheNroQuartos());
                        break;
                    case 10:
                        chacara.setAnoConstrucao(preencheAnoConstrucao());
                        break;
                    case 11:
                        chacara.setDistCidade(preencheDistCidade());
                        break;
                    default:
                        msg("Opcão incorreta!");
                        break;
                }
            } while (opcao != 0);
            if (lista.editar(codigo, chacara)) {
                msg("Chácara editada com sucesso!");
            } else {
                msg("Ocorreu algum erro, tente novamente!");
            }
        }
    }

    /**
     * Metodo responsavel pelo menu principal de pesquisa. Redireciona para o
     * modo de pesquisa que o usuario escolher
     */
    private void pesquisar() {
        int opcao;;

        opcao = inInt("Digite:"
                + "\n 1 - Para pesquisar por codigo"
                + "\n 2 - Para pesquisar por valor"
                + "\n 3 - Para pesquisar por bairro"
                + "\n 0 - Para cancelar");

        switch (opcao) {
            case 0:
                break;
            case 1:
                pesquisarCodigo();
                break;
            case 2:
                pesquisarValor();
                break;
            case 3:
                pesquisarBairro();
                break;
            default:
                msg("Opção incorreta!");
                break;
        }
    }

    /**
     * Metodo responsavel pela pesquisa de uma chácara, pelo seu codigo.
     *
     * Pede ao usuario qual codigo deseja buscar e exibe os dados, caso seja
     * encontrado, da chácara.
     */
    private void pesquisarCodigo() {
        int codigo;

        codigo = preencheCodigo();
        if (codigo < 0) {
            msg("Codigo incorreto!");
        }
        Chacara chacara = (Chacara) lista.consultar(codigo);

        if (chacara == null) {
            msg("Nenhuma chácara encontrada!");
        } else {
            msg(chacara.toString());
        }
    }

    /**
     * Metodo responsavel pela pesquisa de uma chácara, pelo valor.
     *
     * Pede ao usuario qual valor maximo que deseja buscar e exibe a lista de
     * imoveis encontrados.
     */
    private void pesquisarValor() {
        List chacaras;
        double valor;

        valor = inDouble("Digite o valor máximo que deseja pesquisar:");
        chacaras = lista.pesquisaValor(valor);
        exibirChacaras(chacaras);
    }

    /**
     * Metodo responsavel pela pesquisa de uma chacara, pelo bairro.
     *
     * Pede ao usuario qual bairro deseja buscar e exibe a lista de imoveis
     * encontrados.
     */
    private void pesquisarBairro() {
        String bairro;
        List chacaras;

        bairro = preencheBairro();
        chacaras = lista.pesquisaBairro(bairro);
        exibirChacaras(chacaras);
    }

    /**
     * Metodo responsavel pelo menu apos pesquisar e listar.
     */
    private void menuLista() {
        int codigo;

        codigo = inInt("Para exibir mais informações, digite o codigo correspondente à chácara."
                + "\n Digite -1 para sair");
        if (codigo < 0) {
            return;
        }
        Chacara chacara = (Chacara) lista.consultar(codigo);

        if (chacara == null) {
            msg("Nenhuma chácara encontrada!");
        } else {
            msg(chacara.toString());
        }

    }

    /**
     * Metodo responsável por exibir todas as chácaras cadastradas, conforme o
     * usuario desejar.
     */
    private void listar() {
        int opcao;

        opcao = inInt("Digite:"
                + "\n 1 - Para listar por codigo"
                + "\n 2 - Para listar por valor"
                + "\n 3 - Para listar por area");

        switch (opcao) {
            case 0:
                break;
            case 1:
                listarCodigo();
                break;
            case 2:
                listarValor();
                break;
            case 3:
                listarArea();
                break;
            default:
                msg("Opcao incorreta!");
                break;
        }
    }

    /**
     * Metodo responsavel por listar por codigo.
     */
    private void listarCodigo() {
        List chacaras = lista.ordenarCodigo();
        exibirChacaras(chacaras);

    }

    /**
     * Metodo responsavel por ordenar por valor.
     */
    private void listarValor() {
        List chacaras = lista.ordenarValor();
        exibirChacaras(chacaras);
    }

    /**
     * Metodo responsavel por listar por area.
     */
    private void listarArea() {
        List chacaras = lista.ordenarArea();
        exibirChacaras(chacaras);
    }

    /**
     * Metodo responsavel por exibir uma descricao resumida das chacaras
     * contidos na List recebida.
     *
     * @param chacaras List de chacaras para exibir
     */
    private void exibirChacaras(List chacaras) {
        String dados = "";
        Chacara chacara;
        if (chacaras.get(0) == null) {
            msg("Nenhuma chácara encontrado!");
        } else {
            for (Object o : chacaras) {
                chacara = (Chacara) o;
                dados += "Codigo: " + chacara.getCodigo() + " \tCidade:" + chacara.getCidade()
                        + " \tBairro: " + chacara.getBairro() + " \tValor" + chacara.getValor()
                        + " \tArea Total: " + chacara.getAreaTotal() + "m" + "\n";
            }
            msg(dados);
            menuLista();
        }
    }

    //Metodo que lê a base de dados em .csv.
    public void lerDatabase() {
        if (!lista.lerArquivo()) {
            msg("Ocorreu algum erro ao ler a base de arquivos. O arquivo nao existe ou esta corrompido!");
        }
    }

}
