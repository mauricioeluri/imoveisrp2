package Casa;

import Default.ListaImovel;
import static Auxiliares.EntradasTeclado.*;
import static Auxiliares.PreencheCampos.*;
import Default.TipoImovel;
import java.util.List;

/**
 *
 * @author Maurício El Uri
 */
public class Casas {

    private TipoImovel tipo = TipoImovel.CASAS;
    private ListaImovel lista = new ListaImovel(tipo);

    /**
     * Método do menú principal.
     *
     * Deve ser chamado para que alterações possam ser feitas nas casas.
     */
    public void principal() {
        int opcao;
        do {
            opcao = inInt("Digite:"
                    + "\n 1 - Para incluir uma casa"
                    + "\n 2 - Para listar as casas cadastradas"
                    + "\n 3 - Para editar uma casa"
                    + "\n 4 - Para excluir uma casa"
                    + "\n 5 - Para pesquisar"
//                    + "\n 6 - Para ordenar por valor"
                    + "\n 0 - Para sair");

            switch (opcao) {
                case 0:
                    if (!lista.escreverArquivo()) {
                        msg("Ocorreu algum erro ao salvar o arquivo!");
                    }
                    break;
                case 1:
                    incluir();
                    break;
                case 2:
//                    consultarCodigo();
                    listar();
                    break;
                case 3:
                    editar();
                    break;
                case 4:
                    excluir();
                    break;
                case 5:
                    pesquisar();
                    break;
//                case 6:
//                    List casas = lista.ordenarValor();
//                    exibirCasas(casas);
//                    break;
                default:
                    msg("Opção incorreta!");
            }

        } while (opcao != 0);

    }

    /**
     * Método responsavel por solicitar os dados, criar e adicionar na lista uma
     * casa.
     */
    private void incluir() {
        String logradouro, bairro, cidade, descricao;
        int numero, nroQuartos, nroVagasGaragem, anoConstrucao, tipo;
        double areaTotal, valor, areaConstruida;

        logradouro = preencheLogradouro();
        bairro = preencheBairro();
        cidade = preencheCidade();
        descricao = preencheDescricao();
        tipo = preencheTipo();
        areaConstruida = preencheAreaConstruida();
        numero = preencheNumero();
        nroQuartos = preencheNroQuartos();
        nroVagasGaragem = preencheNroVagasGaragem();
        anoConstrucao = preencheAnoConstrucao();
        areaTotal = preencheAreaTotal();
        valor = preencheValor();

        Casa casa = new Casa(tipo, areaConstruida, nroQuartos, nroVagasGaragem,
                anoConstrucao, logradouro, numero, bairro, cidade, descricao,
                areaTotal, valor);

        if (lista.incluir(casa)) {
            msg("Casa adicionada com sucesso!");
            System.out.println(casa.toString());
        } else {
            msg("Ocorreu algum problema, tente novamente!");
        }
    }

    /**
     * Metodo responsável pela consulta de uma casa, pelo seu código.
     *
     * Pede ao usuário qual código deseja buscar e exibe os dados, caso seja
     * encontrado, da casa.
     */
    private void consultarCodigo() {
        int codigo;

        codigo = preencheCodigo();
        Casa casa = (Casa) lista.consultar(codigo);

        if (casa == null) {
            msg("Nenhuma casa encontrada!");
        } else {
            msg(casa.toString());
        }
    }

    /**
     * Metodo responsavel pela exclusão de uma casa, pelo seu código.
     *
     * Pede ao usuário para digitar o código da casa que deseja excluir.
     */
    private void excluir() {
        int codigo;

        codigo = preencheCodigo();
        if (lista.excluir(codigo)) {
            msg("Casa excluída com sucesso!");
        } else {
            msg("Casa não encontrada!");
        }
    }

    /**
     * Metodo responsavel pelo menu principal de pesquisa. Redireciona para o
     * modo de pesquisa que o usuario escolher
     */
    private void pesquisar() {
        int opcao;

        opcao = inInt("Digite:"
                + "\n 1 - Para pesquisar por codigo"
                + "\n 2 - Para pesquisar por valor"
                + "\n 3 - Para pesquisar por bairro"
                + "\n 0 - Para cancelar");

        switch (opcao) {
            case 0:
                break;
            case 1:
                pesquisarCodigo();
                break;
            case 2:
                pesquisarValor();
                break;
            case 3:
                pesquisarBairro();
                break;
            default:
                msg("Opção incorreta!");
                break;
        }
    }

    /**
     * Metodo responsavel pela pesquisa de uma casa, pelo seu codigo.
     *
     * Pede ao usuario qual codigo deseja buscar e exibe os dados, caso seja
     * encontrado, da casa.
     */
    private void pesquisarCodigo() {
        int codigo;

        codigo = preencheCodigo();
        if (codigo < 0) {
            msg("Codigo incorreto!");
        }
        Casa casa = (Casa) lista.consultar(codigo);

        if (casa == null) {
            msg("Nenhuma casa encontrada!");
        } else {
            msg(casa.toString());
        }
    }

    /**
     * Metodo responsavel pela pesquisa de uma casa, pelo valor.
     *
     * Pede ao usuario qual valor maximo que deseja buscar e exibe a lista de
     * imoveis encontrados.
     */
    private void pesquisarValor() {
        List casas;
        double valor;

        valor = inDouble("Digite o valor máximo que deseja pesquisar:");
        casas = lista.pesquisaValor(valor);
        exibirCasas(casas);
    }

    /**
     * Metodo responsavel pela pesquisa de um apartemento, pelo bairro.
     *
     * Pede ao usuario qual bairro deseja buscar e exibe a lista de imoveis
     * encontrados.
     */
    private void pesquisarBairro() {
        String bairro;
        List casas;

        bairro = preencheBairro();
        casas = lista.pesquisaBairro(bairro);
        exibirCasas(casas);
    }

    /**
     * Metodo responsavel por exibir uma descricao resumida das casas contidos
     * na List recebida.
     *
     * @param casas List de casas para exibir
     */
    private void exibirCasas(List casas) {
        String dados = "";
        Casa casa;
        if (casas.get(0) == null) {
            msg("Nenhuma casa encontrada!");
        } else {
            for (Object o : casas) {
                casa = (Casa) o;
                dados += "Codigo: " + casa.getCodigo() + "\tDescrição: " + casa.getDescricao()
                        + " \tBairro: " + casa.getBairro() + " \tValor" + casa.getValor() 
                        + " \tArea Total: " + casa.getAreaTotal() + "m" + "\n";
            }
            msg(dados);
            menuLista();
        }
    }

    /**
     * Metodo responsavel pela edição de uma casa.
     *
     * Pede ao usuário qual o código da casa que deseja editar. Caso seja
     * encontrado, pede qual atributo deseja editar e qual o novo valor do
     * atributo.
     *
     * Salva as alterações após o usuário escolher sair da edição.
     */
    private void editar() {
        Casa casa;
        int codigo, opcao;

        codigo = inInt("Digite o código da casa que deseja editar:");

        casa = (Casa) lista.consultar(codigo);
        if (casa == null) {
            msg("Casa não encontrada!");
        } else {
            do {
                System.out.println(casa.toString());
                opcao = inInt("Digite o numero correspondende ao item que você deseja editar:"
                        + "\n Digite 0 para sair");
                switch (opcao) {
                    case 0:
                        break;
                    case 1:
                        casa.setLogradouro(preencheLogradouro());
                        break;
                    case 2:
                        casa.setNumero(preencheNumero());
                        break;
                    case 3:
                        casa.setBairro(preencheBairro());
                        break;
                    case 4:
                        casa.setCidade(preencheCidade());
                        break;
                    case 5:
                        casa.setDescricao(preencheDescricao());
                        break;
                    case 6:
                        casa.setAreaTotal(preencheAreaTotal());
                        break;
                    case 7:
                        casa.setValor(preencheValor());
                        break;
                    case 8:
                        casa.setTipo(preencheTipo());
                        break;
                    case 9:
                        casa.setAreaConstruida(preencheAreaConstruida());
                        break;
                    case 10:
                        casa.setNroQuartos(preencheNroQuartos());
                        break;
                    case 11:
                        casa.setNroVagasGaragem(preencheNroVagasGaragem());
                        break;
                    case 12:
                        casa.setAnoConstrucao(preencheAnoConstrucao());
                        break;
                    case 13:
                        casa.setLogradouro(preencheLogradouro());
                        break;
                    case 14:
                        casa.setNumero(preencheNumero());
                        break;
                    case 15:
                        casa.setBairro(preencheBairro());
                        break;
                    case 16:
                        casa.setCidade(preencheCidade());
                        break;
                    case 17:
                        casa.setDescricao(preencheDescricao());
                        break;
                    case 18:
                        casa.setAreaTotal(preencheAreaTotal());
                        break;
                    case 19:
                        casa.setValor(preencheValor());
                        break;
                    default:
                        msg("Opcão incorreta!");
                        break;
                }
            } while (opcao != 0);
            if (lista.editar(codigo, casa)) {
                msg("Casa editada com sucesso!");
            } else {
                msg("Ocorreu algum erro, tente novamente!");
            }
        }
    }

    /**
     * Metodo responsavel pelo menu apos pesquisar e listar.
     */
    private void menuLista() {
        int codigo;

        codigo = inInt("Para exibir mais informações, digite o codigo correspondente a casa."
                + "\n Digite -1 para sair");
        if (codigo < 0) {
            return;
        }
        Casa casa = (Casa) lista.consultar(codigo);

        if (casa == null) {
            msg("Nenhuma casa encontrado!");
        } else {
            msg(casa.toString());
        }

    }
    
      /**
     * Metodo responsável por exibir todos as casas cadastradas, conforme
     * o usuario desejar.
     */
    private void listar() {
        int opcao;

        opcao = inInt("Digite:"
                + "\n 1 - Para listar por código"
                + "\n 2 - Para listar por valor"
                + "\n 3 - Para listar por área");

        switch (opcao) {
            case 0:
                break;
            case 1:
                listarCodigo();
                break;
            case 2:
                listarValor();
                break;
            case 3:
                listarArea();
                break;
            default:
                msg("Opcao incorreta!");
                break;
        }
    }
    
    /**
     * Metodo responsavel por listar por codigo.
     */
    private void listarCodigo() {
        List casas = lista.ordenarCodigo();
        exibirCasas(casas);

    }

    /**
     * Metodo responsavel por ordenar por valor.
     */
    private void listarValor() {
        List casas = lista.ordenarValor();
        exibirCasas(casas);
    }

    /**
     * Metodo responsavel por listar por area.
     */
    private void listarArea() {
        List casas = lista.ordenarArea();
        exibirCasas(casas);
    }

    //Metodo que lê a base de dados em .csv.
    public void lerDatabase() {
        if (!lista.lerArquivo()) {
            msg("Ocorreu algum erro ao ler a base de arquivos. O arquivo nao existe ou esta corrompido!");
        }
    }
}
