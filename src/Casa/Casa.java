package Casa;

import Default.Imovel;
import java.io.Serializable;

/**
 *
 * @author Maurício M. El Uri
 */
public class Casa extends Imovel{

    private int tipo;
    private double areaConstruida;
    private int nroQuartos;
    private int nroVagasGaragem;
    private int anoConstrucao;

    /**
     * Construtor de Casa
     *
     * @param tipo
     * @param areaConstruida
     * @param nroQuartos
     * @param nroVagasGaragem
     * @param anoConstrucao
     * @param logradouro
     * @param numero
     * @param bairro
     * @param cidade
     * @param descricao
     * @param areaTotal
     * @param valor
     */
    public Casa(int tipo, double areaConstruida, int nroQuartos,
            int nroVagasGaragem, int anoConstrucao, String logradouro,
            int numero, String bairro, String cidade, String descricao,
            double areaTotal, double valor) {

        super(logradouro, numero, bairro, cidade, descricao, areaTotal, valor);
        this.tipo = tipo;
        this.areaConstruida = areaConstruida;
        this.nroQuartos = nroQuartos;
        this.nroVagasGaragem = nroVagasGaragem;
        this.anoConstrucao = anoConstrucao;
    }
    
    public Casa(int tipo, double areaConstruida, int nroQuartos,
            int nroVagasGaragem, int anoConstrucao, String logradouro,
            int numero, String bairro, String cidade, String descricao,
            double areaTotal, double valor, int codigo) {

        super(logradouro, numero, bairro, cidade, descricao, areaTotal, valor, codigo);
        this.tipo = tipo;
        this.areaConstruida = areaConstruida;
        this.nroQuartos = nroQuartos;
        this.nroVagasGaragem = nroVagasGaragem;
        this.anoConstrucao = anoConstrucao;
    }

    /**
     * @return o tipo, já convertendo o valor de inteiro para String Caso 1:
     * Residencial Caso 2: Comercial
     */
    public String getTipo() {
        if (tipo == 1) {
            return "Residencial";
        }
        return "Comercial";
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the areaConstruida
     */
    public double getAreaConstruida() {
        return areaConstruida;
    }

    /**
     * @param areaConstruida the areaConstruida to set
     */
    public void setAreaConstruida(double areaConstruida) {
        this.areaConstruida = areaConstruida;
    }

    /**
     * @return the nroQuartos
     */
    public int getNroQuartos() {
        return nroQuartos;
    }

    /**
     * @param nroQuartos the nroQuartos to set
     */
    public void setNroQuartos(int nroQuartos) {
        this.nroQuartos = nroQuartos;
    }

    /**
     * @return the nroVagasGaragem
     */
    public int getNroVagasGaragem() {
        return nroVagasGaragem;
    }

    /**
     * @param nroVagasGaragem the nroVagasGaragem to set
     */
    public void setNroVagasGaragem(int nroVagasGaragem) {
        this.nroVagasGaragem = nroVagasGaragem;
    }

    /**
     * @return the anoConstrucao
     */
    public int getAnoConstrucao() {
        return anoConstrucao;
    }

    /**
     * @param anoConstrucao the anoConstrucao to set
     */
    public void setAnoConstrucao(int anoConstrucao) {
        this.anoConstrucao = anoConstrucao;
    }

    @Override
    public String toString() {
        String dados = "";
        dados += super.toString()
                + "\n8 - Tipo: " + this.getTipo()
                + "\n9 - Área Construída: " + this.getAreaConstruida()
                + "\n10 - Número de Quartos: " + this.getNroQuartos()
                + "\n11 - Vagas na Garagem: " + this.getNroVagasGaragem()
                + "\n12 - Ano de Construção: " + this.getAnoConstrucao();
        return dados;
    }

    @Override
    public String toFile() {
        String dados = "";
        dados += super.toFile()
                + this.getTipo() + ";" + this.getAreaConstruida()
                + ";" + this.getNroQuartos() + ";" + this.getNroVagasGaragem()
                + ";" + this.getAnoConstrucao() + ";";
        return dados;

    }

}
