/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Apartamento;

import Default.ListaImovel;
import static Auxiliares.EntradasTeclado.*;
import static Auxiliares.PreencheCampos.*;
import Default.TipoImovel;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Giliardi Schmidt
 *
 * Classe responsável pelas modificações em Imoveis do tipo apartamento.
 */
public class Apartamentos {

    private TipoImovel tipo = TipoImovel.APARTAMENTOS;
    private ListaImovel lista = new ListaImovel(tipo);

    /**
     * Metodo do menu principal.
     *
     * Deve ser chamado para que alterações possam ser feitas nos apartamentos.
     */
    public void principal() {
        int opcao;

        do {
            opcao = inInt("Digite:"
                    + "\n 1 - Para incluir um apartamento"
                    + "\n 2 - Para listar os apartamentos cadastrados"
                    + "\n 3 - Para editar um apartamento"
                    + "\n 4 - Para excluir um apartamento"
                    + "\n 5 - Para pesquisar"
                    + "\n 0 - Para sair");

            switch (opcao) {
                case 0:
                    lista.escreverArquivo();
                    break;
                case 1:
                    incluir();
                    break;
                case 2:
                    listar();
                    break;
                case 3:
                    editar();
                    break;
                case 4:
                    excluir();
                    break;
                case 5:
                    pesquisar();
                    break;
                default:
                    msg("Opção incorreta!");
            }

        } while (opcao != 0);

    }

    /**
     * Metodo responsavel por solicitar os dados, criar e adicionar na lista um
     * apartamento.
     */
    private void incluir() {
        String logradouro, bairro, cidade, descricao, nomeEdificio;
        int numero, nroQuartos, nroVagasGaragem, anoConstrucao, andar, nroApartamento;
        double areaTotal, valor, valorCondominio;

        logradouro = preencheLogradouro();
        bairro = preencheBairro();
        cidade = preencheCidade();
        descricao = preencheDescricao();
        nomeEdificio = preencheNomeEdificio();
        numero = preencheNumero();
        nroQuartos = preencheNroQuartos();
        nroVagasGaragem = preencheNroVagasGaragem();
        anoConstrucao = preencheAnoConstrucao();
        andar = preencheAndar();
        nroApartamento = preencheNroApartamento();
        valorCondominio = preencheValorCondominio();
        areaTotal = preencheAreaTotal();
        valor = preencheValor();

        Apartamento apartamento = new Apartamento(logradouro, numero, bairro, cidade,
                descricao, areaTotal, valor, nroQuartos, nroVagasGaragem, anoConstrucao,
                nomeEdificio, andar, nroApartamento, valorCondominio);

        if (lista.incluir(apartamento)) {
            msg("Apartamento adicionado com sucesso!");
        } else {
            msg("Ocorreu algum problema, tente novamente!");
        }
    }

    /**
     * Metodo responsavel pela exclusao de um apartamento, pelo seu codigo.
     *
     * Pede ao usuario para digitar o codigo do apartamento que deseja excluir.
     */
    private void excluir() {
        int codigo;

        codigo = preencheCodigo();
        if (lista.excluir(codigo)) {
            msg("Apartamento excluido com sucesso!");
        } else {
            msg("Apartamento não encontrado!");
        }
    }

    /**
     * Metodo responsavel pela edição de um apartamento.
     *
     * Pede ao usuario qual o codigo do apartamento que deseja editar. Caso seja
     * encontrado, pede qual atributo deseja editar e qual o novo valor do
     * atributo.
     *
     * Salva as alterações após o usuário escolher sair da edição.
     */
    private void editar() {
        Apartamento apartamento;
        int codigo, opcao;

        codigo = inInt("Digite o codigo do apartamento que deseja editar:");

        apartamento = (Apartamento) lista.consultar(codigo);
        if (apartamento == null) {
            msg("Apartamento não encontrado!");
        } else {
            do {
                System.out.println(apartamento.toString());
                opcao = inInt("Digite o numero correspondende ao item que você deseja editar:"
                        + "\n Digite 0 para sair");

                switch (opcao) {
                    case 0:
                        break;
                    case 1:
                        apartamento.setLogradouro(preencheLogradouro());
                        break;
                    case 2:
                        apartamento.setNumero(preencheNumero());
                        break;
                    case 3:
                        apartamento.setBairro(preencheBairro());
                        break;
                    case 4:
                        apartamento.setCidade(preencheCidade());
                        break;
                    case 5:
                        apartamento.setDescricao(preencheDescricao());
                        break;
                    case 6:
                        apartamento.setAreaTotal(preencheAreaTotal());
                        break;
                    case 7:
                        apartamento.setValor(preencheValor());
                        break;
                    case 8:
                        apartamento.setNroQuartos(preencheNroQuartos());
                        break;
                    case 9:
                        apartamento.setAnoConstrucao(preencheAnoConstrucao());
                        break;
                    case 10:
                        apartamento.setNomeEdificio(preencheNomeEdificio());
                        break;
                    case 11:
                        apartamento.setAndar(preencheAndar());
                        break;
                    case 12:
                        apartamento.setNroApartamento(preencheNroApartamento());
                        break;
                    case 13:
                        apartamento.setValorCondominio(preencheValorCondominio());
                        break;
                    case 14:
                        apartamento.setNroVagasGaragem(preencheNroVagasGaragem());
                        break;
                    default:
                        msg("Opcão incorreta!");
                        break;
                }
            } while (opcao != 0);
            if (lista.editar(codigo, apartamento)) {
                msg("Apartamento editado com sucesso!");
            } else {
                msg("Ocorreu algum erro, tente novamente!");
            }
        }
    }

    /**
     * Metodo responsavel pelo menu principal de pesquisa. Redireciona para o
     * modo de pesquisa que o usuario escolher
     */
    private void pesquisar() {
        int opcao;;

        opcao = inInt("Digite:"
                + "\n 1 - Para pesquisar por codigo"
                + "\n 2 - Para pesquisar por valor"
                + "\n 3 - Para pesquisar por bairro"
                + "\n 0 - Para cancelar");

        switch (opcao) {
            case 0:
                break;
            case 1:
                pesquisarCodigo();
                break;
            case 2:
                pesquisarValor();
                break;
            case 3:
                pesquisarBairro();
                break;
            default:
                msg("Opção incorreta!");
                break;
        }
    }

    /**
     * Metodo responsavel pela pesquisa de um apartemento, pelo seu codigo.
     *
     * Pede ao usuario qual codigo deseja buscar e exibe os dados, caso seja
     * encontrado, do apartamento.
     */
    private void pesquisarCodigo() {
        int codigo;

        codigo = preencheCodigo();
        if (codigo < 0) {
            msg("Codigo incorreto!");
        }
        Apartamento apartamento = (Apartamento) lista.consultar(codigo);

        if (apartamento == null) {
            msg("Nenhum apartamento encontrado!");
        } else {
            msg(apartamento.toString());
        }
    }

    /**
     * Metodo responsavel pela pesquisa de um apartemento, pelo valor.
     *
     * Pede ao usuario qual valor maximo que deseja buscar e exibe a lista de
     * imoveis encontrados.
     */
    private void pesquisarValor() {
        List apartamentos;
        double valor;

        valor = inDouble("Digite o valor máximo que deseja pesquisar:");
        apartamentos = lista.pesquisaValor(valor);
        exibirApartamentos(apartamentos);
    }

    /**
     * Metodo responsavel pela pesquisa de um apartemento, pelo bairro.
     *
     * Pede ao usuario qual bairro deseja buscar e exibe a lista de imoveis
     * encontrados.
     */
    private void pesquisarBairro() {
        String bairro;
        List apartamentos;

        bairro = preencheBairro();
        apartamentos = lista.pesquisaBairro(bairro);
        exibirApartamentos(apartamentos);
    }

    /**
     * Metodo responsavel pelo menu apos pesquisar e listar.
     */
    private void menuLista() {
        int codigo;

        codigo = inInt("Para exibir mais informações, digite o codigo correspondente ao apartamento."
                + "\n Digite -1 para sair");
        if (codigo < 0) {
            return;
        }
        Apartamento apartamento = (Apartamento) lista.consultar(codigo);

        if (apartamento == null) {
            msg("Nenhum apartamento encontrado!");
        } else {
            msg(apartamento.toString());
        }

    }

    /**
     * Metodo responsável por exibir todos os apartametos cadastrados, conforme
     * o usuario desejar.
     */
    private void listar() {
        int opcao;

        opcao = inInt("Digite:"
                + "\n 1 - Para listar por codigo"
                + "\n 2 - Para listar por valor"
                + "\n 3 - Para listar por area");

        switch (opcao) {
            case 0:
                break;
            case 1:
                listarCodigo();
                break;
            case 2:
                listarValor();
                break;
            case 3:
                listarArea();
                break;
            default:
                msg("Opcao incorreta!");
                break;
        }
    }

    /**
     * Metodo responsavel por listar por codigo.
     */
    private void listarCodigo() {
        List apartamentos = lista.ordenarCodigo();
        exibirApartamentos(apartamentos);

    }

    /**
     * Metodo responsavel por ordenar por valor.
     */
    private void listarValor() {
        List apartamentos = lista.ordenarValor();
        exibirApartamentos(apartamentos);
    }

    /**
     * Metodo responsavel por listar por area.
     */
    private void listarArea() {
        List apartamentos = lista.ordenarArea();
        exibirApartamentos(apartamentos);
    }

    /**
     * Metodo responsavel por exibir uma descricao resumida dos apartamentos
     * contidos na List recebida.
     *
     * @param apartamentos List de apartamentos para exibir
     */
    private void exibirApartamentos(List apartamentos) {
        String dados = "";
        Apartamento apartamento;
        if (apartamentos.get(0) == null) {
            msg("Nenhum apartamento encontrado!");
        } else {
            for (Object o : apartamentos) {
                apartamento = (Apartamento) o;
                dados += "Codigo: " + apartamento.getCodigo() + " \tCidade: " + apartamento.getCidade()
                        + " \tBairro: " + apartamento.getBairro() + " \tValor: " + apartamento.getValor()
                        + " \tArea Total: " + apartamento.getAreaTotal() + "m"
                        + "\n";
            }
            msg(dados);
            menuLista();
        }
    }

    //Metodo que lê a base de dados em .csv.
    public void lerDatabase() {
        if (!lista.lerArquivo()) {
            msg("Ocorreu algum erro ao ler a base de arquivos. O arquivo nao existe ou esta corrompido!");
        }
    }
}
