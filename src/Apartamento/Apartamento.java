/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Apartamento;

import Default.Imovel;

/**
 *
 * @author Giliardi Schmidt
 */
public class Apartamento extends Imovel{

    private int nroQuartos;
    private int nroVagasGaragem;
    private int anoConstrucao;
    private String nomeEdificio;
    private int andar;
    private int nroApartamento;
    private double valorCondominio;

    public Apartamento(String logradouro, int numero, String bairro, String cidade,
            String descricao, double areaTotal, double valor, int nroQuartos,
            int nroVagasGaragem, int anoConstrucao, String nomeEdificio,
            int andar, int nroApartamento, double valorCondominio) {

        super(logradouro, numero, bairro, cidade, descricao, areaTotal, valor);
        this.nroQuartos = nroQuartos;
        this.nroVagasGaragem = nroVagasGaragem;
        this.anoConstrucao = anoConstrucao;
        this.nomeEdificio = nomeEdificio;
        this.andar = andar;
        this.nroApartamento = nroApartamento;
        this.valorCondominio = valorCondominio;
    }
    
    public Apartamento(String logradouro, int numero, String bairro, String cidade,
            String descricao, double areaTotal, double valor, int nroQuartos,
            int nroVagasGaragem, int anoConstrucao, String nomeEdificio,
            int andar, int nroApartamento, double valorCondominio, int codigo) {

        super(logradouro, numero, bairro, cidade, descricao, areaTotal, valor, codigo);
        this.nroQuartos = nroQuartos;
        this.nroVagasGaragem = nroVagasGaragem;
        this.anoConstrucao = anoConstrucao;
        this.nomeEdificio = nomeEdificio;
        this.andar = andar;
        this.nroApartamento = nroApartamento;
        this.valorCondominio = valorCondominio;
    }

    public int getNroQuartos() {
        return nroQuartos;
    }

    public void setNroQuartos(int nroQuartos) {
        this.nroQuartos = nroQuartos;
    }

    public int getNroVagasGaragem() {
        return nroVagasGaragem;
    }

    public void setNroVagasGaragem(int nroVagasGaragem) {
        this.nroVagasGaragem = nroVagasGaragem;
    }

    public int getAnoConstrucao() {
        return anoConstrucao;
    }

    public void setAnoConstrucao(int anoConstrucao) {
        this.anoConstrucao = anoConstrucao;
    }

    public String getNomeEdificio() {
        return nomeEdificio;
    }

    public void setNomeEdificio(String nomeEdificio) {
        this.nomeEdificio = nomeEdificio;
    }

    public int getAndar() {
        return andar;
    }

    public void setAndar(int andar) {
        this.andar = andar;
    }

    public int getNroApartamento() {
        return nroApartamento;
    }

    public void setNroApartamento(int nroApartamento) {
        this.nroApartamento = nroApartamento;
    }

    public double getValorCondominio() {
        return valorCondominio;
    }

    public void setValorCondominio(double valorCondominio) {
        this.valorCondominio = valorCondominio;
    }

    @Override
    public String toString() {
        String dados = "";
        dados += super.toString()
                + "\n8 - Numero de Quartos: " + this.nroQuartos
                + "\n9 - Ano de Construção: " + this.anoConstrucao
                + "\n10 - Nome do Edifício: " + this.nomeEdificio
                + "\n11 - Andar: " + this.andar
                + "\n12 - Numero do Apartamento: " + this.nroApartamento
                + "\n13 - Valor do Condominio: " + this.valorCondominio
                + "\n14 - Número de vagas na Garagem: " + this.nroVagasGaragem;

        return dados;
    }

    @Override
    public String toFile() {
        String dados = "";
        dados += super.toFile() + this.nroQuartos + ";" + this.anoConstrucao + ";" + this.nomeEdificio + ";"
                + this.andar + ";" + this.nroApartamento + ";" + this.valorCondominio + ";"
                + this.nroVagasGaragem + ";";
        return dados;
    }
}
