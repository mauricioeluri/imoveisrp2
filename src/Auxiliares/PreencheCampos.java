package Auxiliares;

import static Auxiliares.EntradasTeclado.*;
import java.time.Year;

/**
 *
 * @author Giliardi, Filipe, Maurício, Leônidas
 * Classe que auxilia na entrada de dados do usuário para definir valor aos
 * atributos de cada Imovel.
 */
public abstract class PreencheCampos {

    /**
     * @return Uma String Logradouro válida
     */
    public static String preencheLogradouro() {
        return inString("Digite o logradouro:");
    }

    /**
     * @return Uma String Bairro válida
     */
    public static String preencheBairro() {
        return inString("Digite o bairro:");
    }

    /**
     * @return Uma String Cidade válida
     */
    public static String preencheCidade() {
        return inString("Digite a cidade:");
    }

    /**
     * @return Uma String Descrição válida
     */
    public static String preencheDescricao() {
        return inString("Digite a descrição:");
    }

    /**
     * @return Uma String NomeEdificio válida
     */
    public static String preencheNomeEdificio() {
        return inString("Digite o nome do edifício:");
    }

    /**
     * Pede ao usuário um valor inteiro válido e >= 1 até ser digitado.
     *
     * @return Um inteiro Número (imóvel) válido e >= 1.
     */
    public static int preencheNumero() {
        int in = -1;
        while (in < 1) {
            in = inInt("Digite o número do imóvel:");
        }
        return in;
    }

    /**
     * Pede ao usuário um valor inteiro válido e >= 1 até ser digitado.
     *
     * @return Um inteiro NroQuartos válido e >= 1.
     */
    public static int preencheNroQuartos() {
        int in = -1;
        while (in < 1) {
            in = inInt("Digite o número de quartos: ");
        }
        return in;
    }

       public static int preencheNroBanheiro() {
        int in = -1;
        while (in < 1) {
            in = inInt("Digite o número de Banheiros: ");
        }
        return in;
    }
public static int preencheNroSala() {
        int in = -1;
        while (in < 1) {
            in = inInt("Digite o número da Sala: ");
        }
        return in;
    }

    /**
     * Pede ao usuário um valor inteiro válido e >= 1 até ser digitado.
     *
     * @return Um inteiro NroQuartos válido e >= 1.
     */
    public static int preencheNroVagasGaragem() {
        int in = -1;
        while (in < 0) {
            in = inInt("Digite o número de vagas na garagem:");
        }
        return in;
    }

    /**
     * Pede ao usuário um valor inteiro válido entre 1900 e a data atual até ser
     * digitado.
     *
     * @return Um número inteiro AnoConstrução válido, entre 1900 e a data
     * atual.
     */
    public static int preencheAnoConstrucao() {
        int in = -1;
        while (in < 1900 || in > Year.now().getValue()) {
            in = inInt("Digite o ano de construção:");
        }
        return in;
    }

    /**
     * Pede ao usuário um valor inteiro válido e >= 1 até ser digitado.
     *
     * @return Um número inteiro Andar válido, >= 1.
     */
    public static int preencheAndar() {
        int in = -1;
        while (in < 1) {
            in = inInt("Digite o andar:");
        }
        return in;
    }

    /**
     * Pede ao usuário um valor inteiro válido e > =1 até ser digitado.
     *
     * @return Um número inteiro NroApartamento válido, >= 1.
     */
    public static int preencheNroApartamento() {
        int in = -1;
        while (in < 1) {
            in = inInt("Digite o número do apartamento:");
        }
        return in;
    }

    /**
     * Pede ao usuário um valor inteiro válido e >= 0 até ser digitado.
     *
     * @return Um número inteiro Codigo válido, >= 0.
     */
    public static int preencheCodigo() {
        int in = -1;
        while (in < 0) {
            in = inInt("Digite o código:");
        }
        return in;
    }

    /**
     * Pede ao usuário um valor inteiro válido entre 1 e 2 até ser digitado.
     *
     * @return Um número inteiro Tipo válido, 1 ou 2.
     */
    public static int preencheTipo() {
        int in = -1;
        while (in < 1 || in > 2) {
            in = inInt("[1]Residencial\n[2]Comercial\nDigite o tipo:");
        }
        return in;
    }

    /**
     * Pede ao usuário um valor double válido e >= 1 até ser digitado.
     *
     * @return Um número Double ValorCondominio válido, >= 1.
     */
    public static double preencheValorCondominio() {
        double in = -1;
        while (in < 1) {
            in = inDouble("Digite o valor do condomínio:");
        }
        return in;
    }

    /**
     * Pede ao usuário um valor double válido e >= 1 até ser digitado.
     *
     * @return Um número Double AreaConstruida válido, >= 1.
     */
    public static double preencheAreaConstruida() {
        double in = -1;
        while (in < 1) {
            in = inDouble("Digite a área construída (m):");
        }
        return in;
    }

    /**
     * Pede ao usuário um valor double válido e >= 1 até ser digitado.
     *
     * @return Um número double DistCidade válido, >= 1.
     */
    public static double preencheDistCidade() {
        double in = -1;
        while (in < 1) {
            in = inDouble("Digite a distância da cidade (Km):");
        }
        return in;
    }

    /**
     * Pede ao usuário um valor double válido e >= 1 até ser digitado.
     *
     * @return um número double AreaTotal válido, >= 1.
     */
    public static double preencheAreaTotal() {
        double in = -1;
        while (in < 1) {
            in = inDouble(("Digite a área total (m):"));
        }
        return in;
    }

    /**
     * Pede ao usuário um valor double válido e >= 1 até ser digitado.
     *
     * @return Um número double Valor válido, >= 1.
     */
    public static double preencheValor() {
        double in = -1;
        while (in < 1) {
            in = inDouble("Digite o valor:");
        }
        return in;
    }

    /**
     * Pede ao usuário um valor double válido e >= 1 até ser digitado.
     *
     * @return Um número double comprimento válido >= 1.
     */
    public static double preencheComprimento() {
        double in = -1;
        while (in < 1) {
            in = inDouble("Digite o comprimento:");
        }
        return in;
    }

    /**
     * Pede ao usuário um valor double válido e >= 1 até ser digitado.
     *
     * @return Um número double Largura válido, >= 1.
     */
    public static double preencheLargura() {
        double in = -1;
        while (in < 1) {
            in = inDouble("Digite a largura:");
        }
        return in;
    }
}
