package Auxiliares;

import java.util.Scanner;

/**
 * Classe que auxilia na captura de informações inseridas pelo usuário.
 *
 * @author Giliardi Schmidt
 */
public abstract class EntradasTeclado {

    private static Scanner input = new Scanner(System.in);

    /**
     * Metodo que exibe uma mensagem recebida por parâmetro para o usuário e
     * captura o próximo int que será digitado.
     *
     * @param mensagem Recebe a mensagem que será exibida ao usuário.
     * @return Retorna o int inserido pelo usuário.
     */
    public static int inInt(String mensagem) {
        int in = -1;
        boolean sair = false;

        do {
            msg(mensagem);
            if (input.hasNextInt()) {
                in = input.nextInt();
                input.nextLine();
                sair = true;
            } else {
                msg("Ocorreu algum erro, verifique o que você digitou!");
                input.nextLine();
            }
        } while (sair == false);

        return in;
    }

    /**
     * Método que exibe uma mensagem recebida por parâmetro para o usuário e
     * captura a proxima String que será digitada.
     *
     * @param mensagem Recebe a mensagem que será exibida ao usuário.
     * @return Retorna a String inserida pelo usuário.
     */
    public static String inString(String mensagem) {
        String in = "";
        do {
            msg(mensagem);
            in = input.nextLine();
        } while (in.trim().equals(""));
        
        return in;
    }

    /**
     * Método que exibe uma mensagem recebida por parâmetro para o usuário e
     * captura o próximo double que será digitado.
     *
     * @param mensagem Recebe a mensagem que será exibida ao usuário.
     * @return Retorna o double inserido pelo usuário.
     */
    public static double inDouble(String mensagem) {
        double in = 0;
        boolean sair = false;

        do {
            msg(mensagem);
            if (input.hasNextDouble()) {
                in = input.nextDouble();
                input.nextLine();
                sair = true;
            } else {
                msg("Ocorreu algum erro, verifique o que você digitou!");
                input.nextLine();
            }
        } while (sair == false);

        return in;
    }

    /**
     * Exibe uma mensagem ao usuario, utilizando o div() para melhor
     * visualização.
     *
     * @param mensagem String da mensagem a ser exibida.
     */
    public static void msg(String mensagem) {
        div();
        System.out.println(mensagem);
    }

    /**
     * Metodo que exibe um separador (--- --- ---) para o usuário.
     */
    public static void div() {
        System.out.println("--- --- --- --- --- --- --- --- --- --- --- --- ---");
    }
}
