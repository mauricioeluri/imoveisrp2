package Default;

/**
 *
 * @author Giliardi Schmidt
 * 
 * ENUM responsável pelos tipos de imóveis que usarão a classe ListaImovel
 */
public enum TipoImovel {
    APARTAMENTOS, CASAS, CHACARAS, TERRENOS, SALASCOMERCIAIS;

}
