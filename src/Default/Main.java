package Default;

import Apartamento.Apartamentos;
import Chacara.Chacaras;
import SalaComercial.SalaComerciais;
import static Auxiliares.EntradasTeclado.*;
import Casa.Casas;

/**
 *
 * @author Giliardi Schmidt, Filipe, Mauricio El Uri
 */
public class Main {

    public static void main(String[] args) {
        Apartamentos apartamentos = new Apartamentos();
        Casas casas = new Casas();
        Chacaras chacaras = new Chacaras();
        SalaComerciais salacomerciais = new SalaComerciais();
        int opcao;

        casas.lerDatabase();
        apartamentos.lerDatabase();
        chacaras.lerDatabase();
        salacomerciais.lerDatabase();
        msg("                      SEJA BEM VINDO!");

        do {
            opcao = inInt("Digite:"
                    + "\n 1 - Para gerenciar Apartamentos"
                    + "\n 2 - Para gerenciar Casas"
                    + "\n 3 - Para gerenciar Chácaras"
                    + "\n 4 - Para gerenciar Salas Comerciais"
                    + "\n 0 - Para sair\n");

            switch (opcao) {
                case 0:
                    msg("Fim!");
                    break;
                case 1:
                    apartamentos.principal();
                    break;
                case 2:
                    casas.principal();
                    break;
                case 3:
                    chacaras.menu();
                    break;
                case 4:
                    salacomerciais.principal();
                    break;
                default:
                    msg("Opção incorreta!");
                    break;
            }
        } while (opcao != 0);
    }
}
