package Default;

import ListaLigadaDEC.Lista;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author Giliardi Schmidt, Filipe, Mauricio.
 */
public class ListaImovel implements ListaImoveis {

    private List<Imovel> lista;
    private String diretorio = System.getProperty("user.dir");
    String diretorioArquivo;

    public ListaImovel(TipoImovel tipo) {
        lista = new Lista();
        String arq = tipo.toString();
        this.diretorioArquivo = diretorio + "/Data/" + arq + ".bin";
    }

    @Override
    public boolean incluir(Imovel im) {
        return lista.add(im);
    }

    @Override
    public Imovel consultar(int codigo) {
        int m, e = 0;
        int d = this.lista.size() - 1;
        while (e <= d) {
            m = (e + d) / 2;
            if (this.lista.get(m).getCodigo() == codigo) {
                return lista.get(m);
            } else if (this.lista.get(m).getCodigo() < codigo) {
                e = m + 1;
            } else {
                d = m - 1;
            }
        }
        return null;

    }

    @Override
    public boolean editar(int codigo, Imovel im) {
        int index;
        Imovel i = consultar(codigo);

        if (i != null) {
            index = lista.indexOf(im);
            lista.set(index, im);
            return true;
        }
        return false;
    }

    @Override
    public boolean excluir(int codigo) {
        Imovel i = consultar(codigo);

        if (i != null) {
            lista.remove(i);
            return true;
        }
        return false;
    }

    @Override
    public List<Imovel> ordenarCodigo() {
        return this.lista;
    }

    @Override
    public List<Imovel> ordenarValor() {
        Imovel aux;
        List<Imovel> imoveis = new Lista();
        imoveis.addAll(this.lista);
//        ORDENAÇÃO POR SELEÇÃO
        for (int i = 0; i < imoveis.size(); i++) {
            int menor = i;
            for (int j = i + 1; j < imoveis.size(); j++) {
                if (imoveis.get(j).getValor() < imoveis.get(menor).getValor()) {
                    menor = j;
                }
            }
            aux = imoveis.get(i);
            imoveis.set(i, imoveis.get(menor));
            imoveis.set(menor, aux);
        }
        return imoveis;
    }

    @Override
    public List<Imovel> ordenarArea() {
        Imovel aux;
        List<Imovel> imoveis = new Lista();
        imoveis.addAll(this.lista);
        //Metodo de ordenação por inserção!
        for (int i = 1; i < imoveis.size(); i++) {
            aux = imoveis.get(i);
            for (int j = i - 1; j >= 0 && imoveis.get(j).getAreaTotal() > aux.getAreaTotal(); j--) {
                imoveis.set(j + 1, imoveis.get(j));
                imoveis.set(j, aux);
            }
        }
        return imoveis;
    }

    @Override
    public List<Imovel> pesquisaValor(double valor) {
        List imoveis = new Lista();
        for (Imovel im : lista) {
            if (im.getValor() <= valor) {
                imoveis.add(im);
            }
        }
        return imoveis;

    }

    @Override
    public List<Imovel> pesquisaBairro(String bairro) {
        List imoveis = new Lista();
        for (Imovel im : lista) {
            if (im.getBairro().equalsIgnoreCase(bairro)) {
                imoveis.add(im);
            }
        }
        return imoveis;

    }

    @Override
    public boolean escreverArquivo() {
        File arquivo, pasta;
        ObjectOutputStream output;
        boolean ok;

        try {
            //verifica se a pasta existe, cria ela se precisar
            pasta = new File(this.diretorio + "/Data");
            if (!pasta.exists()) {
                pasta.mkdir();
            }
            //verifica se o arquivo existe, cria ele se precisar
            arquivo = new File(this.diretorioArquivo);
            if (!arquivo.exists()) {
                arquivo.createNewFile();
            }
            output = new ObjectOutputStream(new FileOutputStream(new File(diretorioArquivo)));
            output.writeObject(lista);
            output.close();
            return true;
        } catch (Exception e) {
            ok = false;
        }
        if (ok == false) {
            return false;
        }
        return true;
    }

    @Override
    public boolean lerArquivo() {
        ObjectInputStream input;
        File arquivo;
        int codigo = 0;

        try {
            //verifica se o arquivo existe
            arquivo = new File(this.diretorioArquivo);
            if (!arquivo.exists()) {
                return false;
            }
            //abre o arquivo para leitura
            input = new ObjectInputStream(new FileInputStream(new File(this.diretorioArquivo)));
            this.lista = (Lista<Imovel>) input.readObject();

            for (Imovel imovel : lista) {
                if (imovel.getCodigo() > codigo) {
                    codigo = imovel.getCodigo();
                }
            }

            Imovel.setCodigoImoveis(codigo);

            input.close();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return true;
    }

}
