package Default;

import java.io.Serializable;

/**
 * @authors Maurício M. El Uri, Filipe Garcia, Giliardi Schmidt, Leônidas Ortiz.
 */
public class Imovel implements Serializable {

    private static int codigoImoveis = 0;
    private int codigo;
    private String logradouro;
    private int numero;
    private String bairro;
    private String cidade;
    private String descricao;
    private double areaTotal;
    private double valor;

    /**
     * Construtor de um imóvel
     *
     * @param logradouro
     * @param numero
     * @param bairro
     * @param cidade
     * @param descricao
     * @param areaTotal
     * @param valor
     */
    public Imovel(String logradouro, int numero, String bairro, String cidade, String descricao, double areaTotal, double valor) {
        this.codigo = codigoImoveis;
        this.logradouro = logradouro;
        this.numero = numero;
        this.bairro = bairro;
        this.cidade = cidade;
        this.descricao = descricao;
        this.areaTotal = areaTotal;
        this.valor = valor;
        codigoImoveis++;
    }

    public Imovel(String logradouro, int numero, String bairro, String cidade, String descricao, double areaTotal, double valor, int codigo) {
        this.codigo = codigo;
        this.logradouro = logradouro;
        this.numero = numero;
        this.bairro = bairro;
        this.cidade = cidade;
        this.descricao = descricao;
        this.areaTotal = areaTotal;
        this.valor = valor;
        if (Imovel.codigoImoveis < codigo) {
            Imovel.codigoImoveis = codigo + 1;
        }
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getAreaTotal() {
        return areaTotal;
    }

    public void setAreaTotal(double areaTotal) {
        this.areaTotal = areaTotal;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public int getCodigo() {
        return this.codigo;
    }

    public static void setCodigoImoveis(int codigo) {
        if (codigo >= codigoImoveis) {
            codigoImoveis = codigo + 1;
        }
    }

    @Override
    public String toString() {
        String dados = "";
        dados += "Codigo do Imovel: " + this.codigo
                + "\n1 - Logradouro: " + this.logradouro
                + "\n2 - Numero: " + this.numero
                + "\n3 - Bairro: " + this.bairro
                + "\n4 - Cidade: " + this.cidade
                + "\n5 - Descrição: " + this.descricao
                + "\n6 - Area Total: " + this.areaTotal + "m"
                + "\n7 - Valor: " + this.valor;

        return dados;
    }

    public String toFile() {
        String dados = "";
        dados += this.codigo + ";" + this.logradouro + ";" + this.numero + ";" + this.bairro + ";"
                + this.cidade + ";" + this.descricao + ";" + this.areaTotal + ";" + this.valor + ";";

        return dados;
    }
}
