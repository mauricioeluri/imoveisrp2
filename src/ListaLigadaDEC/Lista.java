package ListaLigadaDEC;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author Giliardi Schmidt, Maurício El Uri, Filipe Garcia
 * @param <T> Tipo da lista.
 */
public class Lista<T> implements List, Serializable, Iterator {

    private int tamanho;
    private No inicio;
    private int indexAtual;

    public Lista() {
        this.inicio = null;
        this.tamanho = 0;

    }

    @Override
    public int size() {
        return this.tamanho;
    }

    @Override
    public boolean isEmpty() {
        return this.tamanho == 0;
    }

    @Override
    public boolean contains(Object o) {
        if (isEmpty()) {
            return false;
        }
        if (o == null) {
            throw new NullPointerException("O objeto não pode ser null!");
        }

        No aux = this.inicio;
        for (int i = 0; i < this.tamanho; i++) {
            if (aux.getInfo().equals(o)) {
                return true;
            }
            aux = aux.getProximo();
        }
        return false;
    }

    @Override
    public Iterator iterator() {
        this.indexAtual = 0;
        return this;
    }

    @Override
    public Object[] toArray() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object[] toArray(Object[] a) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean add(Object e) {
        No<T> no;
        if (e == null) {
            throw new NullPointerException();
        }
        if (this.tamanho == 0) {
            this.inicio = new No(e);
            inicio.setAnterior(this.inicio);
            inicio.setProximo(this.inicio);
            this.tamanho++;
            return true;
        }

        No primeiro = inicio;
        No ultimo = inicio.getAnterior();

        no = new No(e, primeiro, ultimo);
        primeiro.setAnterior(no);
        ultimo.setProximo(no);
        this.tamanho++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        No noProximo;
        No noAnterior;
        if (o == null) {
            throw new NullPointerException();
        }
        if (this.tamanho > 0) {
            No<T> no = this.inicio;
            if (this.tamanho == 1 && (no.getInfo().equals(o))) {
                this.inicio = null;
                this.tamanho--;
                return true;
            }
            for (int i = 0; i < this.tamanho; i++) {
                if (no.getInfo().equals(o)) {
                    noAnterior = no.getAnterior();
                    noProximo = no.getProximo();

                    noAnterior.setProximo(noProximo);
                    noProximo.setAnterior(noAnterior);
                    this.tamanho--;

                    if (i == 0) {
                        this.inicio = noProximo;
                    }
                    return true;
                }
                no = no.getProximo();
            }
        }
        return false;
    }

    public Object getLast() {
        if (this.tamanho <= 0) {
            throw new NullPointerException("Lista Vazia");
        }
        return inicio.getAnterior().getInfo();
    }

    @Override
    public boolean containsAll(Collection c) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean addAll(Collection c) {
        for (Object o : c) {
            this.add(o);
        }
        return true;
    }

    @Override
    public boolean addAll(int index, Collection c) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean removeAll(Collection c) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean retainAll(Collection c) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void clear() {
        this.inicio = null;
        this.tamanho = 0;
    }

    @Override
    public Object get(int index) {
        No aux = this.inicio;
        if (index > this.tamanho || index < 0) {
            throw new IndexOutOfBoundsException("O Index está incorreto.");
        }
        if (this.tamanho == 0) {
            return null;
        }
        if (index == 0) {
            return this.inicio.getInfo();
        }
        for (int i = 0; i < index; i++) {
            aux = aux.getProximo();
        }
        return aux.getInfo();
    }

    @Override
    public Object set(int index, Object element) {
        if (index > this.tamanho || index < 0) {
            throw new IndexOutOfBoundsException("Index informado menor que 0 ou maior que o tamanho da lista!");
        }
        if (element == null) {
            throw new NullPointerException("Objeto não pode ser null!");
        }

        No aux = this.inicio;
        for (int i = 0; i < index; i++) {
            aux = aux.getProximo();
        }
        Object info = aux.getInfo();
        aux.setInfo(element);
        return info;
    }

    @Override
    public void add(int index, Object element) {
        No aux;
        No noAnterior;
        No novo;

        if (index > tamanho || index < 0) {
            throw new IndexOutOfBoundsException("Index Incorreto");
        }
        if (tamanho == 0 && index == 0) {
            inicio = new No(element);
            inicio.setProximo(inicio);
            inicio.setAnterior(inicio);
            this.tamanho++;
        }
        if (index == 0) {
            this.inicio = new No(element);
            this.inicio.setAnterior(inicio);
            this.inicio.setProximo(inicio);
            this.tamanho++;
        } else {
            aux = this.inicio;
            for (int i = 0; i < index; i++) {
                aux = aux.getProximo();
            }
            noAnterior = aux.getAnterior();

            novo = new No(element);
            novo.setAnterior(noAnterior);
            novo.setProximo(aux);

            aux.setAnterior(novo);
            noAnterior.setProximo(novo);
            this.tamanho++;
        }
    }

    @Override
    public Object remove(int index) {
        No aux;
        No auxProximo;
        No auxAnterior;
        Object info;

        if (index > tamanho || index < 0) {
            throw new IndexOutOfBoundsException("O Index está incorreto.");
        } else if (tamanho == 0) {
            throw new NullPointerException("A lista se encontra vazia.");
        } else if (this.tamanho == 1 || (index == 0 && this.tamanho == 1)) {
            info = inicio.getInfo();
            inicio = null;
            this.tamanho--;
            return info;
        } else if (index == 0) {
            info = inicio.getInfo();
            auxProximo = inicio.getProximo();
            auxAnterior = inicio.getAnterior();
            auxProximo.setAnterior(auxAnterior);
            auxAnterior.setProximo(auxProximo);
            inicio = auxProximo;
            this.tamanho--;
            return info;
        } else {
            aux = this.inicio;
            for (int i = 0; i < index; i++) {
                aux = aux.getProximo();
            }
            info = aux.getInfo();
            auxAnterior = aux.getAnterior();
            auxProximo = aux.getProximo();
            auxAnterior.setProximo(auxProximo);
            auxProximo.setAnterior(auxAnterior);
            this.tamanho--;
            return info;
        }
    }

    @Override

    public int indexOf(Object o) {
        if (o == null) {
            throw new NullPointerException("O objeto não pode ser null!");
        }

        No<T> no = this.inicio;
        for (int i = 0; i < this.tamanho; i++) {
            if (no.getInfo().equals(o)) {
                return i;
            }
            no = no.getProximo();
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o
    ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ListIterator listIterator() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ListIterator listIterator(int index
    ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List subList(int fromIndex, int toIndex
    ) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Object getFirst() {
        if (inicio == null) {
            throw new IndexOutOfBoundsException("Lista Vazia");
        } else {
            return inicio.getInfo();
        }
    }

    public void addFirst(Object o) throws NullPointerException {
        No no = new No(o, null, null);

        if (no == null) {
            throw new NullPointerException("Entrada invalida!");

        } else if (this.isEmpty()) {
            no.setProximo(no);
            no.setAnterior(no);
            inicio = no;
            tamanho++;
        } else {
            no.setProximo(inicio);
            no.setAnterior(inicio.getAnterior());
            inicio.getAnterior().setProximo(no);
            inicio.setAnterior(no);
            inicio = no;
            tamanho++;
        }
    }

    public Object removeFirst() {
        Object aux;
        if (this.isEmpty()) {
            throw new IndexOutOfBoundsException("Lista Vazia");
        } else if (inicio == inicio.getAnterior()) {
            aux = inicio.getInfo();
            inicio = null;
            tamanho--;
            return aux;
        } else {
            aux = inicio.getInfo();
            inicio.getAnterior().setProximo(inicio.getProximo());
            inicio.getProximo().setAnterior(inicio.getAnterior());
            inicio = inicio.getProximo();
            tamanho--;
            return aux;
        }
    }

    @Override
    public boolean hasNext() {
        return (this.indexAtual < this.tamanho);
    }

    @Override
    public Object next() {
        T result = (T) get(indexAtual);
        this.indexAtual++;
        return result;
    }

    /**
     * Classe interna, responsavel pelo nodo.
     *
     * @param <T> Tipo de informacao que sera gravada no No
     */
    private class No<T> implements Serializable {

        private T info;
        private No proximo;
        private No anterior;

        public No(T info, No proximo, No anterior) {
            this.info = info;
            this.proximo = proximo;
            this.anterior = anterior;
        }

        public No(T info) {
            this.info = info;
        }

        public Object getInfo() {
            return info;
        }

        public void setInfo(T info) {
            this.info = info;
        }

        public No getProximo() {
            return proximo;
        }

        public void setProximo(No proximo) {
            this.proximo = proximo;
        }

        public No getAnterior() {
            return anterior;
        }

        public void setAnterior(No anterior) {
            this.anterior = anterior;
        }

    }

}
